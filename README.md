bikh - do not use it, it's not alpha yet
========================================
**bikh** - **B**ash **I**s **K**illing **H**orza

This is by no means anywhere near alpha stage

It is going to be a general-purpose Bash framework
The main idea is to incorporate main facilities such as logging, error handling, time, timing, emailing and similar facilities, as well as some more advanced stuff such as module-handling and kinda object-oriented aproach functionalities.

This should make my life a bit easier when writing backup scripts, all sorts of deployment scripts and stuff like that.
It should be compatible with bash on ALL major linux distros, and cygwin. Possibly bsd too.

I'm writing it in my spare time, and once I'm satisfied with the main stuff, I'll update this readme and try to get some more people into this "project".

Words like "proactive", "streamline", "synergy" and "enterprise" are never to be used by people who work on this thing. This is just a spare-time project which is supposed to make life easier and should not be considered serious or corporate or whatever in any way.
